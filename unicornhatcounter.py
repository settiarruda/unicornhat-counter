try:
    from unicornhatsim import unicornhat as uh
except:
    import unicornhat as uh

import time


def timer(sec):
    line = 0
    color = 255 / sec
    red = 255
    blue = 0

    def start(sec):
        for t in range(0, sec):
            nonlocal line
            if (t != 0) and (t % 8 == 0):
                if line >= 7:
                    uh.off()
                    line = 0
                else:
                    line += 1
                sec = sec - 8
                start(sec)
                break
            nonlocal red, blue
            uh.set_pixel(t, line, red, blue, 0)
            # color
            red = red - color
            blue = blue + color
            print(t, line, sec)
            uh.show()
            time.sleep(0.05)

    start(sec)


timer(72)
